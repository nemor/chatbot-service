# import sys
# print(sys.path)
# uvicorn main:app --reload
from fastapi import FastAPI, WebSocket, File, UploadFile, Header, Form
from starlette.websockets import WebSocketDisconnect
from fastapi.middleware.cors import CORSMiddleware
import ai.asr.asr as asr
import utils.strings as strings
import os
import ai.chat.v2.chat as chat

# import ai.chat.service as chatService
# import ai.chat.utils as utils
# import ai.asr.asr_stream as asrStream
# from functools import partial

app = FastAPI()
# 允许所有来源的跨域请求
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# def sendText(msg):
#     print(f'123{msg}\n')
#     # await socket.send_text(msg)

@app.websocket("/websocket")
async def websocket_endpoint(websocket: WebSocket):
    try:
        await websocket.accept()
        while True:
            data = await websocket.receive_json()
            print(data['content'])
            chat.send = websocket.send_text
            # chat.send = sendText
            await chat.on_message(data['content'])
            # await websocket.send_text(result)
    except WebSocketDisconnect:
        # WebSocket connection closed
        pass

# @app.websocket("/websocket1")
# async def websocket_endpoint1(websocket: WebSocket):
#     try:
#         await websocket.accept()
#         while True:
#             # file = await websocket.receive_bytes()
#             data = await websocket.receive_json()
#             print(data['content'])
#             # async def send(msg):
#             #     websocket.send_text(msg)
#             result = chatService.chat_with_rwkv(data['content'])
#             await websocket.send_text(result)
#             # print(data)
#             # await websocket.send_json(data)
#             # await websocket.send_json(f"Message text was: {data}")
#     except WebSocketDisconnect:
#         # WebSocket connection closed
#         pass


@app.get("/")
async def root():
    return {"version": "0.1"}


@app.post("/record")
async def postRecord(username: str = Form(...), file: UploadFile = File(...), token: str = Header(None)):
    # # Do something with the uploaded file
    # data = await file.read()
    tmpFile = "/tmp/{}.ogg".format(strings.generate_random_string(10))
    # name = "/home/yang/record.ogg"
    # print(name)
    # # print(data)
    # # result = asr(file)
    # # 保存文件到指定目录
    # with open(name, "wb") as f:
    #     contents = await file.read()
    #     f.write(contents)
    # # asrStream.recognize(data)
    # result = asr.recognize(name)
    # return {"data": 1111, "code": 200, "msg": "success"}
    with open(tmpFile, "wb") as f:
        contents = await file.read()
        f.write(contents)
    result = asr.recognize(tmpFile)
    os.remove(tmpFile)
    if not result : 
        return {"data": "", "code": 201, "msg": "error"}
    else:
        return {"data": result, "code": 200, "msg": "success"} 


@app.post("/json")
async def postJson(body: dict, token: str = Header(None)):
    print(body)
    print(token)
    # spp.recognize()
    return {"code": 200, "msg": "success"}


@app.post("/login")
async def login(body: dict):
    return {"code": 200, "msg": "success"}


@app.post("register")
async def register(body: dict):
    return {"code": 200, "msg": "success"}

