# chatbot-service

#### 介绍
nemor是基于ChatRWKV和PPSAR制作的聊天app，这是该程序的服务端。


#### 使用说明

1.  conda install paddlepaddle==2.4.2 --channel https://mirrors.aliyun.com/anaconda/cloud/Paddle/
2.  pip install -r requirements.txt

#### QQ群

![QQ群](./resources/qq.jpg)


