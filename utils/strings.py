import random
import string


def generate_random_string(length):
    # 生成包含大小写字母和数字的字符集
    characters = string.ascii_letters + string.digits
    # 从字符集中随机选择length个字符，并拼接成字符串
    random_string = ''.join(random.choice(characters) for i in range(length))
    return random_string
