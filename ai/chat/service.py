import os, types, time, sys, gc
import torch
from torch.utils.cpp_extension import CUDA_HOME
from flask import Flask, jsonify, request
from rwkv.model import RWKV
from rwkv.utils import PIPELINE, PIPELINE_ARGS

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.append(f'{current_path}/../rwkv_pip_package/src')

torch.backends.cudnn.benchmark = True
torch.backends.cudnn.allow_tf32 = True
torch.backends.cuda.matmul.allow_tf32 = True

os.environ["RWKV_JIT_ON"] = '1' # '1' or '0', please use torch 1.13+ and benchmark speed
os.environ["RWKV_CUDA_ON"] = '0' # '1' to compile CUDA kernel (10x faster), requires c++ compiler & cuda libraries

CHAT_LANG = 'Chinese' # English // Chinese // more to come
args = types.SimpleNamespace()
args.strategy = 'cuda fp16i8 *10 -> cuda fp16'
if CHAT_LANG == 'English':
    args.MODEL_NAME = '/fsx/BlinkDL/HF-MODEL/rwkv-4-raven/RWKV-4-Raven-14B-v9-Eng99%-Other1%-20230412-ctx8192'
elif CHAT_LANG == 'Chinese': # Raven系列可以对话和 +i 问答。Novel系列是小说模型，请只用 +gen 指令续写。
    # args.MODEL_NAME = '/home/yang/models/RWKV-4-Raven-7B-v12-Eng49%-Chn49%-Jpn1%-Other1%-20230530-ctx8192.pth'
    args.MODEL_NAME = '/home/yang/models/RWKV-4-Raven-3B-v12-Eng49%-Chn49%-Jpn1%-Other1%-20230527-ctx4096.pth'

gc.collect()
torch.cuda.empty_cache()

# 检测 CUDA 是否可用并输出 CUDA 设备名称和 CUDA 安装路径
if torch.cuda.is_available():
    device_name = torch.cuda.get_device_name(0)
    print("CUDA device found:", device_name)

    cuda_home = CUDA_HOME
    if cuda_home is None or cuda_home.strip() == '':
        print("CUDA_HOME is empty, please check your CUDA driver")
    else:
        os.environ['CUDA_HOME'] = cuda_home
        print("CUDA home:", cuda_home)
else:
    print("CUDA device not found")

# 输出 "loading model"，加载模型并输出 "model loaded"
print("loading model...")
model = RWKV(model=args.MODEL_NAME, strategy=args.strategy)
print("model loaded")

END_OF_TEXT = 0
END_OF_LINE = 187
END_OF_LINE_DOUBLE = 535

args = PIPELINE_ARGS(temperature = 1.0, top_p = 0.7, top_k=0, # top_k = 0 then ignore
                     alpha_frequency = 0.25,
                     alpha_presence = 0.25,
                     token_ban = [0], # ban the generation of some tokens
                     token_stop = [END_OF_TEXT], # stop generation whenever you see any token here
                     chunk_len = 256) # split input into chunks to save VRAM (shorter -> slower)

pipeline = PIPELINE(model, "20B_tokenizer.json")
AVOID_REPEAT = '，：？！'
AVOID_REPEAT_TOKENS = []
for i in AVOID_REPEAT:
    dd = pipeline.encode(i)
    assert len(dd) == 1
    AVOID_REPEAT_TOKENS += dd

def my_print(s):
    print(s, end='', flush=True)

# 修改 /chatrwkv 路由，同时支持 GET 和 POST 请求
def chat_with_rwkv(message):
    msg = message.replace('\\n','\n').strip()
    # 构建聊天历史记录文件名和路径
    filename = f"tmp.txt"
    filepath = os.path.join(os.path.dirname(__file__), 'history', filename)

    # 如果聊天历史记录文件不存在，则创建文件
    if not os.path.exists(filepath):
        open(filepath, 'w').close()

    # 将消息内容写入聊天历史记录文件
    with open(filepath, 'a', encoding='utf-8') as f:
        f.write(f"{msg}\n")

    # 从聊天历史记录文件中读取上下文内容并加载上下文
    # with open(filepath, 'r', encoding='utf-8') as f:
    #     context = f.read()
    # model.loadContext(newctx=context)

    # 调用 RWKV 模型进行聊天
    # output, state = model.forward([187, 510, 1563, 310, 247], None)
    
    # out = pipeline.generate(msg,  )
    # pipeline.generate(msg, token_count=200, args=args, callback=handle)
    result = pipeline.generate(msg, token_count=200, args=args)
    gc.collect()
    torch.cuda.empty_cache()
    return result
    # res = output

    # 将聊天结果写入聊天历史记录文件
    # with open(filepath, 'a', encoding='utf-8') as f:
    #     f.write(f"{res}\n")

    # 将聊天结果写入响应中并返回
    # return out


# chat_with_rwkv("水煮肉片的做法", my_print)
# print(result)