from queue import Queue

class MessageQueue(object):
    def __init__ (self, websocket) :
        # 创建一个队列对象
        self.queue = Queue()  

    def put(self, msg):
        self.queue.put(msg)

    def run(self):
        # while not self.queue.empty():
        while True:
            print(self.queue.get())
