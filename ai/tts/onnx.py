# 参考：【PaddleSpeech】语音合成-onnx模型 - https://aistudio.baidu.com/aistudio/projectdetail/4402081?sUid=2470186&shared=1&ts=1665469316638
# 句子合成速度达到毫秒级
import onnxruntime as ort
from paddlespeech.server.utils.util import denorm
from paddlespeech.t2s.frontend.zh_frontend import Frontend
import numpy as np
import soundfile as sf
import time
import IPython.display as dp
import math
from paddlespeech.server.utils.util import get_chunks
from past.builtins import raw_input

voc_block = 36
voc_pad = 14
am_block = 72
am_pad = 12
voc_upsample = 300

phones_dict = "/media/yang/749E70729E702EAC/develop-tools/Anaconda3/paddlespeech/fastspeech2_cnndecoder_csmsc_streaming_onnx_1.0.0/phone_id_map.txt"
# phones_dict = "/media/yang/749E70729E702EAC/develop-tools/Anaconda3/paddlespeech/fastspeech2_mix_onnx_0.2.0/phone_id_map.txt"
frontend = Frontend(phone_vocab_path=phones_dict, tone_vocab_path=None)


# 模型路径
onnx_am_encoder = "/media/yang/749E70729E702EAC/develop-tools/Anaconda3/paddlespeech/fastspeech2_cnndecoder_csmsc_streaming_onnx_1.0.0/fastspeech2_csmsc_am_encoder_infer.onnx"
onnx_am_decoder = "/media/yang/749E70729E702EAC/develop-tools/Anaconda3/paddlespeech/fastspeech2_cnndecoder_csmsc_streaming_onnx_1.0.0/fastspeech2_csmsc_am_decoder.onnx"
onnx_am_postnet = "/media/yang/749E70729E702EAC/develop-tools/Anaconda3/paddlespeech/fastspeech2_cnndecoder_csmsc_streaming_onnx_1.0.0/fastspeech2_csmsc_am_postnet.onnx"
onnx_voc_melgan = "/media/yang/749E70729E702EAC/develop-tools/Anaconda3/paddlespeech/mb_melgan_csmsc_onnx_0.2.0/mb_melgan_csmsc.onnx"
am_stat_path = "/media/yang/749E70729E702EAC/develop-tools/Anaconda3/paddlespeech/fastspeech2_cnndecoder_csmsc_streaming_onnx_1.0.0/speech_stats.npy"
# mixed_encoder = "/media/yang/749E70729E702EAC/develop-tools/Anaconda3/paddlespeech/fastspeech2_mix_onnx_0.2.0/fastspeech2_mix.onnx"
am_mu, am_std = np.load(am_stat_path)
# 用CPU推理
providers = ['CPUExecutionProvider']
# 配置ort session
sess_options = ort.SessionOptions()
# 创建session
am_encoder_infer_sess = ort.InferenceSession(onnx_am_encoder, providers=providers, sess_options=sess_options)
am_decoder_sess = ort.InferenceSession(onnx_am_decoder, providers=providers, sess_options=sess_options)
am_postnet_sess = ort.InferenceSession(onnx_am_postnet, providers=providers, sess_options=sess_options)
voc_melgan_sess = ort.InferenceSession(onnx_voc_melgan, providers=providers, sess_options=sess_options)
# mixed = ort.InferenceSession(mixed_encoder, providers=providers, sess_options=sess_options)


# 推理阶段封装
# 端到端合成：一次性把句子全部合成完毕
def inference(text):
    phone_ids = frontend.get_input_ids(text, merge_sentences=True, get_tone_ids=False)['phone_ids']
    orig_hs = am_encoder_infer_sess.run(None, input_feed={'text': phone_ids[0].numpy()})
    # orig_hs = mixed.run(None, input_feed={'text': phone_ids[0].numpy()})
    hs = orig_hs[0]
    am_decoder_output = am_decoder_sess.run(None, input_feed={'xs': hs})
    am_postnet_output = am_postnet_sess.run(None, input_feed={
        'xs': np.transpose(am_decoder_output[0], (0, 2, 1))
    })
    am_output_data = am_decoder_output + np.transpose(am_postnet_output[0], (0, 2, 1))
    normalized_mel = am_output_data[0][0]
    mel = denorm(normalized_mel, am_mu, am_std)
    wav = voc_melgan_sess.run(output_names=None, input_feed={'logmel': mel})[0]
    return wav


def depadding(data, chunk_num, chunk_id, block, pad, upsample):
    """
    Streaming inference removes the result of pad inference
    """
    front_pad = min(chunk_id * block, pad)
    # first chunk
    if chunk_id == 0:
        data = data[:block * upsample]
    # last chunk
    elif chunk_id == chunk_num - 1:
        data = data[front_pad * upsample:]
    # middle chunk
    else:
        data = data[front_pad * upsample:(front_pad + block) * upsample]

    return data


def inference_stream(text):
    input_ids = frontend.get_input_ids(
        text,
        merge_sentences=True,  # 是否按符号拆分句子,貌似合成速度快了一点点
        get_tone_ids=False)
    phone_ids = input_ids["phone_ids"]
    print(phone_ids)
    for i in range(len(phone_ids)):
        # 先分句
        # am
        voc_chunk_id = 0
        orig_hs = am_encoder_infer_sess.run(None, input_feed={'text': phone_ids[i].numpy()})
        orig_hs = orig_hs[0]

        # streaming voc chunk info
        mel_len = orig_hs.shape[1]
        voc_chunk_num = math.ceil(mel_len / voc_block)
        start = 0
        end = min(voc_block + voc_pad, mel_len)

        # streaming am
        hss = get_chunks(orig_hs, am_block, am_pad, "am")
        am_chunk_num = len(hss)
        for i, hs in enumerate(hss):
            am_decoder_output = am_decoder_sess.run(
                None, input_feed={'xs': hs})
            am_postnet_output = am_postnet_sess.run(
                None,
                input_feed={
                    'xs': np.transpose(am_decoder_output[0], (0, 2, 1))
                })
            am_output_data = am_decoder_output + np.transpose(
                am_postnet_output[0], (0, 2, 1))
            normalized_mel = am_output_data[0][0]

            sub_mel = denorm(normalized_mel, am_mu, am_std)
            sub_mel = depadding(sub_mel, am_chunk_num, i,
                                am_block, am_pad, 1)

            if i == 0:
                mel_streaming = sub_mel
            else:
                mel_streaming = np.concatenate(
                    (mel_streaming, sub_mel), axis=0)

            # streaming voc
            # 当流式AM推理的mel帧数大于流式voc推理的chunk size，开始进行流式voc 推理
            while (mel_streaming.shape[0] >= end and
                   voc_chunk_id < voc_chunk_num):
                voc_chunk = mel_streaming[start:end, :]

                sub_wav = voc_melgan_sess.run(
                    output_names=None, input_feed={'logmel': voc_chunk})
                sub_wav = depadding(
                    sub_wav[0], voc_chunk_num, voc_chunk_id,
                    voc_block, voc_pad, voc_upsample)

                yield sub_wav

                voc_chunk_id += 1
                start = max(
                    0, voc_chunk_id * voc_block - voc_pad)
                end = min(
                    (voc_chunk_id + 1) * voc_block + voc_pad,
                    mel_len)


'''
这个23秒长的句子，合成时间约在1.8秒:
据封面新闻21日报道，记者从家属方获悉，该事件已初步达成和解。21日，孩子母亲王蕾（化名）称，安徽医科大学第一附属医院约见了前夫陈冬（化名），已初步达成协议，两人对孩子的感情不会改变。孩子被教育得非常好，只不过我们最初是想要自己的孩子，不是通过别人捐赠胚胎的方式。
'''
# num = 0
# while num < 5:
#     text = raw_input("请输入内容：")
#     t1 = time.time()
#     wav = inference(text)
#     sf.write("nice_" + str(num) + ".wav", data=wav, samplerate=24000)
#     print("合成耗时1：", time.time() - t1)
#     num += 1

    # wavs = []
    # t1 = time.time()
    # for sub_wav in inference_stream(text):
    #     print("响应时间：", time.time() - t1)
    #     # t1 = time.time()
    #     wavs.append(sub_wav.flatten())
    # wav = np.concatenate(wavs)
    # t2 = time.time()
    # # sf.write("news1.wav", data=wav, samplerate=24000)
    # sf.write("news.wav", data=wav, samplerate=24000)
    # print("合成耗时1：", time.time() - t1)
    # print("合成耗时2：", time.time() - t2)
    # num += 1
    # dp.display()
