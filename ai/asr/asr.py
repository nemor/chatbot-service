# https://gitee.com/yeyupiaoling/PPASR?_from=gitee_search
import time
from ppasr.predict import PPASRPredictor

# https://github.com/PaddlePaddle/PaddleSpeech/blob/develop/docs/source/released_model.md
# 好像这些模型都差不多，识别率都一般吧
predictor = PPASRPredictor(model_tag='conformer_streaming_fbank_wenetspeech')
# predictor = PPASRPredictor(model_path='/home/yang/.cache/ppasr/asr1_chunk_conformer_wenetspeech_ckpt_1.0.0a/exp/chunk_conformer/checkpoints/avg_10.pdparams',
#                            use_gpu=True)
# predictor = PPASRPredictor(model_tag='asr1_chunk_conformer_wenetspeech_ckpt_1.0.0a')
# predictor = PPASRPredictor(model_path='/home/yang/.cache/ppasr/asr0_deepspeech2_online_wenetspeech_ckpt_1.0.4.model/exp/deepspeech2_online/checkpoints/avg_10.pdparams',
#                            use_gpu=True)
# predictor = PPASRPredictor(model_path='/home/yang/.cache/ppasr/wav2vec2ASR-large-aishell1_ckpt_1.4.0/exp/wav2vec2ASR/checkpoints/avg_10.pdparams',
#                            use_gpu=True)
# predictor = PPASRPredictor(configs='configs/deepspeech2.yml',
#                            model_path='models/deepspeech2_streaming_fbank/infer/',
#                            use_gpu=True)

# wav_path = "/home/yang/test.wav"

def recognize(audio_data):
    try:
        t1 = time.time()
        result = predictor.predict_long(audio_data=audio_data, use_pun=False)
        score, text = result['score'], result['text']
        print(f"识别结果: {text}, 得分: {score}")
        print(time.time() - t1)
        return text
    except IndexError:
        # WebSocket connection closed
        return ""



# convert(wav_path)
# import time
# from paddlespeech.cli.asr import ASRExecutor
#
# asr = ASRExecutor()
# t1 = time.time()
# result = asr(lang='zh',audio_file="zh.wav")
# print(time.time() - t1)
# print(result)
