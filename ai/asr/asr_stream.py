import time
import wave

from ppasr.predict import PPASRPredictor

predictor = PPASRPredictor(model_tag='conformer_online_fbank_wenetspeech')

# 识别间隔时间
interval_time = 0.5
CHUNK = int(16000 * interval_time)


# # 读取数据
# wav_path = '/home/yang/nice.wav'
# wf = wave.open(wav_path, 'rb')
# data = wf.readframes(CHUNK)


def recognize(data):
    # 播放
    # while data != b'':
    position = 0
    while len(data) - position * CHUNK > CHUNK:
        start = time.time()
        d = data[position * CHUNK:position * CHUNK + CHUNK]
        print(position)
        result = predictor.predict_stream(audio_data=data, use_pun=False)
        position += 1
        if result is None:
            continue
        score, text = result['score'], result['text']
        print(f"【实时结果】：消耗时间：{int((time.time() - start) * 1000)}ms, 识别结果: {text}, 得分: {int(score)}")
    # 重置流式识别
    # predictor.reset_stream()

# # 识别间隔时间
# interval_time = 0.5
# CHUNK = int(16000 * interval_time)
# # 读取数据
# wav_path = '/home/yang/nice.wav'
# wf = wave.open(wav_path, 'rb')
# data = wf.readframes(CHUNK)
# # 播放
# while data != b'':
#     start = time.time()
#     d = wf.readframes(CHUNK)
#     result = predictor.predict_stream(audio_data=data, use_pun=False, is_end=d == b'')
#     data = d
#     if result is None: continue
#     score, text = result['score'], result['text']
#     print(f"【实时结果】：消耗时间：{int((time.time() - start) * 1000)}ms, 识别结果: {text}, 得分: {int(score)}")
# # 重置流式识别
# predictor.reset_stream()
