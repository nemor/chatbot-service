# import paddlehub as hub
#
# # 加载语音识别模型
# # asr_model = hub.Module(name="deepspeech2_librispeech")
# # asr_model = hub.Module(name="deepspeech2_aishell")
# asr_model = hub.Module(name="u2_conformer_wenetspeech")
#
# # # 读取音频文件
# # audio_path = "test.wav"
# # with open(audio_path, "rb") as f:
# #     audio_data = f.read()
#
# # 设置识别参数
# config = {
#     "model": "deepspeech2_librispeech",
#     "alpha": 1.2,
#     "beta": 0.35,
#     "beam_size": 500,
#     "num_processes": 1,
#     "num_samples_per_slice": 32000,
#     "num_results": 1,
#     "timeout": -1,
# }
#
#
# def recognize(audio_data):
#     # with open(audio_path, "rb") as f:
#     #     audio_data = f.read()
#     # 开始流式识别
#     print(audio_data)
#     return asr_model.streaming_recognize(audio_data, config)
